var s, maggiObj = {
    
    settings: {
        //newtask        : $("ol#newtask"),
        body            : jQuery('body'),
        scene           : jQuery('.scene'),
        maggi_menu      : jQuery('#maggi-menu'),
        maggi_menu_btn  : jQuery('.mmenu-btn'),
        mm_close        : jQuery('#maggi-menu .mm-close'),
        home_slider1    : jQuery('#jam-presentation .mm-close'),
    },
            
    events : {        
        'window' :{
            'resize': 'resize'
        },

        '.mmenu-btn':{
            'click': 'openmmenuHandle'
        },

        '#maggi-menu .mm-close': {
            'click' : 'closemenuHandle'
        }
    },
    
    init: function() {        
        s = this.settings;        
        this.applyBehavior();
        //this.parallaxHome();
        this.resize();
        this.mmenuHandle(); 
        this.bxsliderHome();


    },     
    
    parallaxHome: function(){
        s.scene.parallax({
            calibrateX: false,
            calibrateY: true,
            invertX: false,
            invertY: true,
            limitX: false,
            limitY: false,
            scalarX:5,
            scalarY: 15,
            frictionX: 0.2,
            frictionY: 0.1,
            originX:0.0,
            originY:2.0
          });
    },

    resize: function(){
        //s.scene[0].style.width = window.innerWidth - 15 + 'px';
        //s.scene[0].style.height = window.innerHeight + 'px';

    },

    bxsliderHome: function(){

        var w = jQuery(window).width();
        console.log(w);


        if(s.body.hasClass('home')){

            var slider = $('.bxslider').bxSlider({
                    onSliderLoad: function(currentIndex){
                        $('.bxslider>li').eq(currentIndex + 1).addClass('animated');
                    },
                    onSlideNext: function($slideElement, oldIndex, newIndex){
                        $($slideElement).addClass('animated');
                    },
                    onSlidePrev: function($slideElement, oldIndex, newIndex){
                        $($slideElement).addClass('animated');
                    }
                }
            );


        }

    },

    mmenuHandle: function(){
      s.maggi_menu.mmenu({
          offCanvas : {
          position : "top",
          zposition : "front"
         }
      });

    },

    openmmenuHandle:function(){
        maggiObj.mmenuHandle();
    },

    closemenuHandle: function(e){
        e.preventDefault();
        s.maggi_menu.trigger("close.mm");
    },





    //Apply Events            
    applyBehavior : function() {
        for (var target in this.events) {
            for (var key in this.events[target]) {
                var method  = this.events[target][key];
                var match   = key.match(/^(\S+)\s*(.*)$/);
                var eventName = match[1], selector = match[2];
                
                if (selector === '') {
                    if (target === 'window')
                        $(window).on(eventName, this[method]);    
                    else
                        $(target).on(eventName, this[method]);
                } else {
                    $(target).on(eventName, selector, this[method]);
                }
            }
        }
    }
    
    

}; //end Obj

maggiObj.init();


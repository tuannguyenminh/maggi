/*-------------------------------------------------------------------------------------------
JAVASCRIPT "sia"

Version:    1.0 - 2013
author:     Burocratik (alexandre gomes)
email:      alex@burocratik.com
website:    http://www.burocratik.com
--------------------------------------------------------------------------------------------*/

$(document).ready(function(){
    var H=calculateHeight(), W=calculateWidth();

/********************************************************************************************
=COLHERES
*********************************************************************************************/
/*-----------------------------------------------------------------------
=Subir e descer o bloco-3 Bolas && $btnChooseSpoon.click and ESC & Menu pequenos
-----------------------------------------------------------------------*/
    var $btnSpoonBloco=$(".btnCreate a"), $spoonBloco=$("#spoons");
    var audioBatatas= $("#audioBatatas")[0];

    $btnSpoonBloco.toggle(
        function(){
            var W=calculateWidth(), H=calculateHeight(), auxMargin="-405px";
            if(W<=1150 || H<=936) {auxMargin="-365px";}else{auxMargin="-405px";};
            if(H>936) auxMargin="-405px";
            $spoonBloco.stop().animate({"margin-top": auxMargin}, 600, "easeOutBack", function(){
                $spoonBloco.addClass("on");
            });
            $(".btnCreate").hide();
            $("#selo").addClass("go");
            if(H<=810) {
                $("body").addClass("smallH");
                fHideMenu(true);
            }; //end small screens
            return false;
        },
        function(){
            var W=calculateWidth(), H=calculateHeight(), auxMargin="-245px";
            (W<=1150 || H<=936) ? auxMargin="-230px" : auxMargin="-245px";
            if(H>936) auxMargin="-245px";
             //Fechar 3 Bolas e colheres (1Âº ver se um estÃ¡ aberto)
            if( $("#spoons .escolha.on").length ) {
                $("#spoons .escolha.on").find("h4 a.btn").trigger("clickTouchEvent");
            };
            //
            $spoonBloco.removeClass("on");
            $spoonBloco.stop().animate({"margin-top": auxMargin}, 600, "easeOutBack");
            $("body").removeClass("smallH");
            fHideMenu(false);
            return false;
        }
    );//end click

///////
    $btnSpoonBloco.hover(
        function () {
             $(this).parent().css("-webkit-animation-play-state", "paused").css("animation-play-state", "paused");
             $("#create .btnCreateShadow").css("-webkit-animation-play-state", "paused").css("animation-play-state", "paused");
         },
         function () {
            $(this).parent().css("-webkit-animation-play-state", "running").css("animation-play-state", "running");
            $("#create .btnCreateShadow").css("-webkit-animation-play-state", "running").css("animation-play-state", "running");
        }
    );//end hover to stop animation

    $btnSpoonBloco.on("touchend",function(){
        $(this).parent().css("-webkit-animation-play-state", "running").css("animation-play-state", "running");
        $("#create .btnCreateShadow").css("-webkit-animation-play-state", "running").css("animation-play-state", "running");
        return false;
    });//end click


    $(".hideNav a").on("clickTouchEvent",function(event){
        $btnSpoonBloco.click();
        event.stopPropagation(); event.preventDefault();
    });//end small screens


/*-----------------------------------------------------------------------
=Abrir/fechar as Bolas + Rodar grupo de colheres (flavours)
-----------------------------------------------------------------------*/
    var $btnOpenSpoon=$("#spoonsWrapper h4 a.btn");

    $btnOpenSpoon.on("clickTouchEvent",function(){
        if( !$spoonBloco.hasClass("on") ) return false;

        $("#spoonsWrapper h4 a.btnspin").show();
        if( $(this).hasClass("btnspin") ) return false; // se Ã© o botao rodar

        ////
        var $clicadoGrupo=$(this).parents(".escolha");
        fOpenCloseSpoonsGroup($clicadoGrupo);
        return false;
    });//end click

//Flavours (rodar grupo)
    _globalClickRodaSpoons=0; _globalMustSpinSpoons=false; _globalMustSpinSpoonsDeg=0;
    $("#spoonsWrapper h4 a.btnspin").on("clickTouchEvent",function(){
        ++_globalClickRodaSpoons;
        _globalMustSpinSpoonsDeg=_globalClickRodaSpoons*180;
        //multiplo de 360
        var remainder = _globalMustSpinSpoonsDeg % 360;
        if (remainder == 0){_globalMustSpinSpoons=false;}else{_globalMustSpinSpoons=true;}
        $(this).parent().next(".spin").css( "transform", "rotate("+_globalMustSpinSpoonsDeg+"deg)" );
        return false;
    });//end click

/*-----------------------------------------------------------------------



/*-----------------------------------------------------------------------
=Escolher colheres e over (Hover e click tem de ser no <a>)
-----------------------------------------------------------------------*/
    var $btnChooseSpoon=$("#spoons .escolha li a");
    //$btnChooseSpoon.click(function(){
    $btnChooseSpoon.on("clickTouchEvent",function(){
        var $this=$(this);
        //to slideup bloco 3 escolhas
        if( !$("#spoons").hasClass("on") ) {
            $btnSpoonBloco.click();
            $.doTimeout(650, function(){$this.parents(".escolha").find("h4 a.btn").trigger("clickTouchEvent");});
            return false;
        };
        //if( $this.parent().hasClass("selected") ) return false; //nao deixar clicar - old version
        var $all=$this.parents("ul").children("li");
        var $clicadoGrupo=$this.parents(".escolha");
        var indice=$this.parent().index();
        $all.attr("class","");
        $this.parent().addClass("selected");
        //Abrir colheres = clicar na bola (so abro colheres se o grupo estiver fechado)
        if( $("#spoons").hasClass("on") &&  !$clicadoGrupo.hasClass("on") ){
            $clicadoGrupo.find("h4 a.btn").trigger("clickTouchEvent");
            return false;
        };
        //
        if( $this.parents(".escolha").hasClass("lots")){
            if(!$this.parents(".escolha").hasClass("on")) return false;//TIRAR QUANDO TIVER COLHER VAZIA??!!!
            if( $this.parents("#flavours").length ) fanimateSabores(indice);
        }else{
            fOpenCloseSpoonsGroup($clicadoGrupo);
            //
            if( $this.parents("#frying").length ) fanimateOleos(indice);
            if( $this.parents("#chips").length ) fanimateSacos(indice);
        };
        return false;
    });//end click


    $btnChooseSpoon.hover(
        function () {
            if( $(this).parent().hasClass("selected") && $("#spoons").hasClass("on")  ) return false;
            $(this).parent().addClass("over");
        },
        function () {
            $(this).parent().removeClass("over");
        }
    );//end hover


/********************************************************************************************
=KEYS
*********************************************************************************************/
  $(document).on("keyup", function(e) {
      if (e.which == 27) {
        //Fechar 3 Bolas e colheres (1Âº ver se um estÃ¡ aberto)
        if( $("#spoons").hasClass("on") ) {
            $btnSpoonBloco.click();
        };//end if
      };
  });



/********************************************************************************************
=SECTION INFINITO de SABORES
*********************************************************************************************/
    var listaInfinito=new Array();
    var textoStart=$("#infinite ul li.c1 span").html();
    smallClientsVar = [
        { cL:10, cT: 100}, { cL:130, cT: 65}, { cL:235, cT: 35},
        { cL:342, cT: 15}, { cL:445, cT: 10}, { cL:545, cT: 15},
        { cL:650, cT: 35}, { cL:760, cT: 65}, { cL:860, cT: 100}
    ];


    $("#infinite ul li em").mouseenter(function(){
        var $this=$(this);
        var cordX=$this.parent().position().left;
        //texto
        var texto=$this.children("span").html();
        $("#colheresMsg em").html(texto);
        //Bola
        if ( !$("html").hasClass("mobile") ) $this.parent("li").addClass("on");
        var i=$this.parent("li").index();
        $("#colheresMsg").stop().animate({"left": smallClientsVar[i].cL, "top":smallClientsVar[i].cT }, 800, "easeOutExpo");
    }); // end

    $("#infinite ul li em").mouseleave(function(){
        var $this=$(this);
        if ( !$("html").hasClass("mobile") ) $this.parent("li").removeClass("on");
    }); // end

    $("#infinite .colheres").mouseleave(function(){
        $.doTimeout(500, function(){
            $("#colheresMsg em").html(textoStart);
            $("#colheresMsg").stop().animate({"left": smallClientsVar[4].cL, "top":smallClientsVar[4].cT }, 800, "easeOutExpo");
        });
    }); // end



/*-------------------------------------------------------------------------
=EVENTS
-------------------------------------------------------------------------*/
    var auxY=$("#receita").offset().top;
    var y = $(window).scrollTop();  
     
    if ( $("html").hasClass("mobile") || $("html").hasClass("ff2") ) {$("#receita").addClass("goAnim");};
    (y>=auxY) ? goPrato() : "";

    $(window).on("scroll", function(){
        y = $(window).scrollTop();
        // Animation small
        if (y>=auxY) {goPrato();}
    })    
    
    // $(window).on("touchmove", function(){
    //     y = $(window).scrollTop();
    //     // Animation small
    //     (y>=auxY) ? $("#receita").addClass("goAnim") : "";
    // })

    // with debounce for Receitas
    $(window).scroll($.debounce(1000, function(){
        var auxY=$("#receita").offset().top;
        gotoReceitas(y, auxY);
    }));//end scroll
    $(window).resize($.debounce(1000, function(){
        var auxY=$("#receita").offset().top;
        var y = $(window).scrollTop();
        gotoReceitas(y, auxY);
    }));//end resize

    function goPrato(){
        if( !$("#receita").hasClass("goAnim") ){
            $.doTimeout(800, function(){$("#pratos .right").animatePNG(179, 20, 20, 0);})
            $.doTimeout(1300, function(){$("#pratos .left").animatePNG(179, 19, 20, 0);})
        }
        $("#receita").addClass("goAnim");
    }//end function
/////////////////////////
})//end load document


/********************************************************************************************
=COLHERES - FUNCTIONS
*********************************************************************************************/

/*-----------------------------------------------------------------------
=Fechar/abrir GRUPO de Bolas (2 situacoes: fecho a propria ou se outra esta aberta fechar a outra)
-----------------------------------------------------------------------*/
    function fOpenCloseSpoonsGroup($clicadoGrupo){
        var $grupo=$("#spoons .escolha"), $wasOpen=null;
        //estava ja algum aberto?
        $grupo.each(function(){
            if( $(this).hasClass("on") ) $wasOpen=$(this);
        });//end each
        //Flavours
        if(_globalMustSpinSpoons){
            var aux=_globalMustSpinSpoonsDeg-180;
            $("#spoons .escolha ul.spin").css( "transform", "rotate("+aux+"deg)" );
            _globalMustSpinSpoons=false;  _globalClickRodaSpoons=_globalClickRodaSpoons-1;
            $.doTimeout(500, function(){fOpenCloseSpoonsGroupBecauseFlavours($clicadoGrupo, $grupo, $wasOpen)});
        }else{
            fOpenCloseSpoonsGroupBecauseFlavours($clicadoGrupo, $grupo, $wasOpen);
        };
    };//end function

    function fOpenCloseSpoonsGroupBecauseFlavours($clicadoGrupo, $grupo, $wasOpen){
        var quantos=fContarLots();
        if( $clicadoGrupo.hasClass("on") ) { //se jÃ¡ estÃ¡ aberto (no caso de flavours so posso fechar indirectamente via baixar o bloco de spoons)
            $clicadoGrupo.removeClass("on");
            fSpoonSlectedEnd($clicadoGrupo.find("li"));
            //if( quantos == 1 &&  $clicadoGrupo.attr("id")=="flavours" ) $("#noflavour").fadeTo(100,1).css("z-index", 30); //end flavours with none selected when i click to close "crie as suas batatas"
            if( quantos == 1 &&  $clicadoGrupo.attr("id")=="flavours" && $("#noflavour").hasClass("selected") ) $("#noflavour").fadeTo(100,1).css("z-index", 30); //close flavours with no selected when click to close "crie as suas batatas"
        }else{
            $grupo.removeClass("on");
            $clicadoGrupo.addClass("on");
            $clicadoGrupo.find("li").fadeTo(0,1);
            if($wasOpen!=null) fSpoonSlectedEnd($wasOpen.find("li"));
            //flavours
            if( $clicadoGrupo.attr("id")=="flavours") $("#noflavour").fadeTo(0,0).css("z-index", 0);
            if( quantos == 1 && $("#noflavour").hasClass("selected")) {
                if( $wasOpen!=null && $wasOpen.attr("id")=="flavours") $("#noflavour").fadeTo(100,1).css("z-index", 30);
            }//end flavours with none selected
        };//end if
    };//end function

        //flavours

/*-----------------------------------------------------------------------
=Fechar as colheres
-----------------------------------------------------------------------*/
    function fSpoonSlectedEnd($elements){
        $elements.on(global_transitionsEvent, function(){
            $elements.off();//remove event
            $elements.each(function(){
                if( !$(this).hasClass("selected") ) $(this).fadeTo(120,0);
            });//end each
        }); //end on


    };//end function

/*-----------------------------------------------------------------------
=Flavours, contar selected not more of 5 (total 6-1(empty spoon)
-----------------------------------------------------------------------*/
    function fContarLots(){
        var $elem=$("#spoons .escolha.lots li"), i=0;
        $elem.each(function(){
            if( $(this).hasClass("selected") ) ++i;
        });//end each
        if (i>1) $("#noflavour").fadeTo(0,0).css("z-index", 0);
        return i;
    }

/*-----------------------------------------------------------------------
=Oleos - animacao das imagens
-----------------------------------------------------------------------*/
    function fanimateOleos(indice){
        $("#fryingPics li.in").removeClass("in").addClass("out");
        $("#fryingPics li:eq("+indice+")").removeClass("out").addClass("in");
        // icons
        $("#selo .oleo p span, #selo .oleoFrases span").removeClass("selected");
        $("#selo .oleo p span:eq("+indice+"), #selo .oleoFrases span:eq("+indice+")").addClass("selected");
    };//end function

/*-----------------------------------------------------------------------
=SACOS - animacao dos sacos imagens
-----------------------------------------------------------------------*/
    var audioBatatas0= $("#audioBatatas0")[0], audioBatatas1= $("#audioBatatas1")[0], audioBatatas2= $("#audioBatatas2")[0];
    _global_vimdaPalha=null;
    function fanimateSacos(indice){
        var $saco=$("#saco");
        var yi= -658, yiout= 1000;
        var W=calculateWidth(), H=calculateHeight(), yf= 158;
        if(W<=1150 || H<=925) {yf= 90;}else{yf= 158;};

        $("#audioBatatas"+indice+"")[0].play();

        $saco.stop().animate({"top": yiout}, 500, "easeInExpo", function(){
            $(this).attr("class","");
            $(this).addClass("bg"+indice+"");
            $(this).css("top",yi);
            fanimateSacosIN($saco, yf);
        });
        // PALHA nao tem Multiplos sabores
        if (indice == 2) {
            $("#flavours a").css("pointer-events", "none");
            $("#flavours .cantDo").show();
            fanimateSabores(8);
            _global_vimdaPalha=true;
        }else{
            $("#flavours a").css("pointer-events", "auto");
            $("#flavours .cantDo").hide();
             if(_global_vimdaPalha) {
                _global_vimdaPalha=null;
                $("#flavours").removeClass("on");
                $("#flavours .spin li").removeClass("selected");
                $("#noflavour").addClass("selected").fadeTo(100,1).css("z-index", 30);
            }
        }; // end if palha
    };//end function

    function fanimateSacosIN($saco, yf){
        $saco.stop().animate({"top": yf}, 800, "easeOutElasticBURO");
    };//end function

/*-----------------------------------------------------------------------
=FLAVOURS - animacao das imagens
-----------------------------------------------------------------------*/
    function fanimateSabores(indice){
        //var boleano=true;
        var $texto=$("#selo .sabores");
        var aux = $("#flavours .spin li:eq("+indice+") img").attr("alt");

        $texto.addClass("go");
        $.doTimeout(600, function(){
          $texto.text(aux);
          $texto.removeClass("txtSmall");
          if( $("body.en").length && indice==0 ) $texto.addClass("txtSmall");
          if( $("body.fr").length && indice==1 ) $texto.addClass("txtSmall");
        });
        $.doTimeout(1300, function(){$texto.removeClass("go");});
        //anima elementos
        $("#flavoursPics li").removeClass("on");
        $("#flavoursPics li:eq("+indice+")").addClass("on");
    };//end function

/********************************************************************************************
=SECTION RECEITAS
*********************************************************************************************/
    function gotoReceitas(yDoc, posicao){
        if(yDoc>posicao-300 && yDoc<posicao+300){
            $("html, body").stop().animate({"scrollTop": posicao+18}, 300, "easeInOutQuint");
        }
    };


/********************************************************************************************
=OTHER - FUNCTIONS
*********************************************************************************************/
/*-----------------------------------------------------------------------
=small screens hide navigation
-----------------------------------------------------------------------*/
    function fHideMenu(bol){
        if(bol){
            $("header nav").hide();
            // $("header nav").fadeTo(300,0,function(){ $(this).hide();});
        }else{
            if ( $("html").hasClass("mobile") ){
                $.doTimeout(100, function(){$("header nav").show();}); //confito e ia para sobre no mobile
            }else{
                $("header nav").show();
            }
            //$("header nav").fadeTo(300,1,function(){$(this).show();});
        };//endif
    };//end function






